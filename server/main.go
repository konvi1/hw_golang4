package main

/*
Напишите сервер для нахождения общих символов двух строк с
авторизацией Basic auth и клиент, выполняющий запрос к серверу.

Условия:

метод POST На входе
{
  "str1": "wejnef2",
  "str2": "wdqwew3"
}
На выходе
{
  "res": "we"
}
авторизация в слое middleware

*/

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-chi/chi"
)

const (
	str1Key  = "str1"
	str2Key  = "str2"
	user     = "test"
	password = "test"
)

type Result struct {
	Res string `json:"res"`
}

type BodyRequest struct {
	Str1 string `json:"str1"`
	Str2 string `json:"str2"`
}

// наличие элемента в слайсе
func isFindNumInSlice(sl []int, elem int) bool {
	for _, vnum1 := range sl {
		if vnum1 == elem {
			return true
		}
	}
	return false
}

// поиск общих символов в 2х строках
func findCommonChars(str1, str2 string) string {
	str1Rune := []rune(str1)
	str2Rune := []rune(str2)

	slInxSl2 := make([]int, 0, len(str2Rune))
	// слайс индексов из второго слайса, где было совпадение элементов
	//	strRuneOut := make([]rune, len(str1Rune))
	strRuneOut := make([]rune, 0)

	for _, v1 := range str1Rune {
		for iSl2, v2 := range str2Rune {
			if v1 == v2 && !isFindNumInSlice(slInxSl2, iSl2) {
				slInxSl2 = append(slInxSl2, iSl2)
				strRuneOut = append(strRuneOut, v1)
				break
			}
		}
	}

	return string(strRuneOut)
}

func main() {
	router := newRouter()

	srv := newServer(router, ":85")

	go func() {
		log.Print("server start")
		err := srv.ListenAndServe()
		if err != nil {
			log.Fatal(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	srv.Shutdown(ctx)
}

func newServer(r http.Handler, addr string) *http.Server {
	return &http.Server{
		Handler: r,
		Addr:    addr,
	}
}

func newRouter() http.Handler {
	r := chi.NewRouter()

	r.Use(auth)

	r.Post("/", func(w http.ResponseWriter, r *http.Request) {

		w.Header().Add("Content-type", "application/json")

		reqBody, err := io.ReadAll(r.Body)

		if err != nil {
			log.Fatal(err)
		}

		if err != nil {
			log.Print("can't read bodyByte", err)
			wrapError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		var reqBodyStruct BodyRequest
		err = json.Unmarshal(reqBody, &reqBodyStruct)
		if err != nil {
			log.Print("can't Unmarshal reqBodyStruct", err)
			wrapError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		str1 := reqBodyStruct.Str1
		str2 := reqBodyStruct.Str2

		resStruct := Result{
			Res: findCommonChars(str1, str2),
		}

		resByte, err := json.Marshal(resStruct)
		if err != nil {
			log.Print("can't marshal resStruct", err)
			wrapError(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Write(resByte)

	})
	return r
}

type Error struct {
	Error string `json:"error"`
}

func wrapError(w http.ResponseWriter, err string, code int) {
	resByte, errMarshal := json.Marshal(Error{
		Error: err,
	})
	if errMarshal != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(code)
	w.Write(resByte)

}

func auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		u, p, ok := r.BasicAuth()
		if !ok {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		if u != user || p != password {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w, r)
	})
}
