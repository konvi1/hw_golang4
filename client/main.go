package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

const (
	URL      = "http://127.0.0.1:85/"
	user     = "test"
	password = "test"
)

type BodyRequest struct {
	Str1 string `json:"str1"`
	Str2 string `json:"str2"`
}

type Result struct {
	Res string `json:"res"`
}

type Error struct {
	Error string `json:"error"`
}

func main() {

	dataBoby := BodyRequest{
		Str1: "wejnef2",
		Str2: "wdqwew3",
	}

	dataBobyByte, err := json.Marshal(dataBoby)
	if err != nil {
		log.Print("can't marshal dataBobyByte", err)
		return
	}

	req, err := http.NewRequest(http.MethodPost, URL, bytes.NewBuffer(dataBobyByte))
	if err != nil {
		log.Fatal(err)
	}

	req.SetBasicAuth(user, password)

	cli := http.Client{}

	resp, err := cli.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()
	res, err := io.ReadAll(resp.Body)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(res))

}
