## Вопросы

* для чего предназначена функция runtime.GOMAXPROCS(n int)

Необходимо для задания кол-во одновременных горутин.

* какое значение по умолчанию, если не делать явный вызов runtime.GOMAXPROCS(n int)

По умолчанию равно кол-ву ядер на компьютере.

* приведите пример, при котором можно улучшить производительность с помощью метода runtime.GOMAXPROCS(n int)

Если необходимо, чтобы какая-то функция или горутина выполнилась быстро и никто не мешал (не переключалось на выполнение другой горутины), можно установить GOMAXPROCS(1)

* что достаточно добавить в следующем примере, чтобы "hello world" никогда не вывелось. Поправьте пример
```go
package main

import (
	"fmt"
	"runtime"
    "time"
)

func main() {
	runtime.GOMAXPROCS(1)
	go func() {
		fmt.Println("hello world")
	}()
    time.Sleep(5 * time.Second)
}
```

## Решение
```go
package main

import (
	"fmt"
	"runtime"
    "time"
)

func main() {
	runtime.GOMAXPROCS(1)
	go func() {
		fmt.Println("hello world")
	}()
    for {

    }
}
```

